#include <iostream>
#include <iomanip>

/* ��� � 1-1 */

//���������� ���������, ������������ ����������� ������� board
const int hor = 5, ver = 5;

using namespace std;

//����� �������, ����������� ��������� �����, �� �����
void printBoard(int array[][ver])
{
	cout << endl;
	for (int j = 0; j < ver; j++)
	{
		for (int i = 0; i < hor; i++)
			cout << setw(4) << array[i][j];

		cout << endl << endl;
	}
}

int main()
{
	setlocale(0, "");
	//�������� ������ ��� �������, ������������ ��������� �����
	int board[hor][ver] = { 0 };

	// ������
	board[1][1] = -1;
	board[0][3] = -1;
	board[2][4] = -1;
	board[4][3] = -1;

	//��������� ����� ����� ��������� �������� ����� ����� �� �����(�� ����� ������ �� 0 �� 7)
	int horizontal[8] = { 2, 1, -1, -2, -2, -1, 1, 2 };
	int vertical[8] = { -1, -2, -2, -1, 1, 2, 2, 1 };
	/*
	//������ ����������� ����� �����
	int accessibility[hor][ver] = { 
		{ 2, 3, 4, 4, 4, 4, 3, 2 },
		{ 3, 4, 6, 6, 6, 6, 4, 3 },
		{ 4, 6, 8, 8, 8, 8, 6, 4 },
		{ 4, 6, 8, 8, 8, 8, 6, 4 },
		{ 4, 6, 8, 8, 8, 8, 6, 4 },
		{ 4, 6, 8, 8, 8, 8, 6, 4 },
		{ 3, 4, 6, 6, 6, 6, 4, 3 },
		{ 2, 3, 4, 4, 4, 4, 3, 2 } 
	};*/

	int accessibility[hor][ver] = { 0 };
	
	int accessRow, accessColumn;

	// �������� ������� �����������
	for (int i = 0; i < hor; i++){
		for (int j = 0; j < ver; j++){
			for (int z = 0; z <= 7; z++){
				accessRow = i + horizontal[z];
				accessColumn = j + vertical[z];

				if (accessRow >= 0 && accessRow <= hor - 1 && accessColumn >= 0 && accessColumn <= ver - 1){
					accessibility[i][j]++;
				}
			}
		}
	}

	//����������, ������������ ������� ���������� ���������� ����
	int currentRow, currentColumn;
	//����������, ������������ ������� ���� �����(�� 0 �� 7)
	int moveNumber;
	//������� ����� �����
	int counter = 0;

	do{
		//����������� ����� ���������� ���������� ���� �� �����������
		do{
			cout << "������� ���� �� �����������. ����������(1 - " << hor << "): ";
			cin >> currentRow;
		} while (currentRow < 1 || currentRow > hor);

		//���� ���������� �� ���������
		do{
			cout << "������� ���� �� ���������. ����������(1 - " << ver << "): ";
			cin >> currentColumn;
		} while (currentColumn < 1 || currentColumn > ver);
	} while (board[currentRow-1][currentColumn-1] == -1);

	int mainRow = currentRow - 1, mainColumn = currentColumn - 1;
	//���������� ��� ������ ��������� ���������� ���� �����
	int Row, Column;

	//�������� ����� �������
	for (int i = 1; i <= hor*ver; i++)
	{
		board[mainRow][mainColumn] = ++counter;

		//������ ������ ����������� ��������� �����������
		int minAccessibility = 6;
		//��������� ���������� ��� ������������ ���������������
		int minA = 6, minB = 6;

		//���������� ����������� �� ������� ������, ������������� � ����� ���� �� ���������
		for (moveNumber = 0; moveNumber <= 7; moveNumber++)
		{
			//���������� ��������� ���� �� ����� ����� ������������ ��� ��������� ������������
			currentRow = mainRow;
			currentColumn = mainColumn;

			//��������� ����������� ���� ��������� ����� �� ��������� �� ������� �����
			currentRow += horizontal[moveNumber];
			currentColumn += vertical[moveNumber];

			//�� ������� �� �� ������� �����
			if (currentRow >= 0 && currentRow <= hor-1 && currentColumn >= 0 && currentColumn <= ver-1)
			{
				//��������� ����������� ������ � ����� ����
				accessibility[currentRow][currentColumn]--;

				//���� ����������� ������, �� ������ �� �������
				if (minAccessibility > accessibility[currentRow][currentColumn] && board[currentRow][currentColumn] == 0)
				{
					//����� ����������� �����������
					minAccessibility = accessibility[currentRow][currentColumn];

					//��������� ���������� ��� ���������� ����������� ����������� � ���, ��� ������
					int RowA = currentRow, ColumnA = currentColumn;

					for (int moveA = 0; moveA <= 7; moveA++)
					{
						RowA += horizontal[moveA];
						ColumnA += vertical[moveA];

						if (RowA >= 0 && RowA <= hor-1 && ColumnA >= 0 && ColumnA <= ver-1)
						{
							if (minA >= accessibility[RowA][ColumnA] && board[RowA][ColumnA] == 0)
								//���������� ����������� ���������� ����
								minA = accessibility[RowA][ColumnA];
						}
					}

					//����������� ���������� � ���� �� ��� ������
					Row = currentRow;
					Column = currentColumn;
				}

				//���� ����������� �����
				if (minAccessibility == accessibility[currentRow][currentColumn] && board[currentRow][currentColumn] == 0)
				{
					minAccessibility = accessibility[currentRow][currentColumn];

					//��������� ���������� ��� ���������� ����������� ����������� � ���, ��� �����
					int RowB = currentRow, ColumnB = currentColumn;

					for (int moveB = 0; moveB <= 7; moveB++)
					{
						RowB += horizontal[moveB];
						ColumnB += vertical[moveB];

						if (RowB >= 0 && RowB <= hor-1 && ColumnB >= 0 && ColumnB <= ver-1)
						{
							if (minB >= accessibility[RowB][ColumnB] && board[RowB][ColumnB] == 0)
								//���������� ����������� ���������� ����
								minB = accessibility[RowB][ColumnB];
						}
					}

					//���� �� ������ �� ��� ��� �� ������ �� ��� ���
					if (board[currentRow][currentColumn] == 0 && minB < minA)
					{
						//�������� �������������� ��� ���������� ���� ���������� � ������, ���� ����������� ���������� ���� ������
						Row = currentRow;
						Column = currentColumn;
					}
				}
			}
		}

		if (Row == 0 && Column == 0){
			cout << endl << "*** ����� �� " << i << " ����! ***" << endl;
			break;
		}
		mainRow = Row;
		mainColumn = Column;

		Row = Column = 0;
	}

	//����� ������� ��� ������ �������, ������������� ��������� �����
	printBoard(board);

	cin.get();
	cin.get();
	return 0;
}